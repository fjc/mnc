# My Next Cron

Retruns the number of seconds until my next cron job.

## Usage

MNC[]	=> seconds until next my cron job [from now]

MNC[now]=> seconds until next my cron job from now

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
