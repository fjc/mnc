# frozen_string_literal: true

require 'test_helper'

class MNCParsletTest < Minitest::Test
  def test_it_does_something_useful
    load 'mnc/parslet.rb'

    parslet = MNC::Parslet.new

    assert_equal '*/5',   parslet.entry   .parse('*/5')  .to_s
    assert_equal '*/5',   parslet.step    .parse('*/5')  .to_s
    assert_equal '*',     parslet.entry   .parse('*')    .to_s
    assert_equal '*',     parslet.wildcard.parse('*')    .to_s
    assert_equal '2-4',   parslet.entry   .parse('2-4')  .to_s
    assert_equal '2-4',   parslet.range   .parse('2-4')  .to_s
    assert_equal '2,4',   parslet.entry   .parse('2,4')  .to_s
    assert_equal '2,4',   parslet.list    .parse('2,4')  .to_s
    assert_equal '2,4-6', parslet.entry   .parse('2,4-6').to_s
    assert_equal '2,4-6', parslet.list    .parse('2,4-6').to_s

    c = <<~CRONTAB
      00 * * * * ...
      30 6 * * * ...
      00,15,30,45 6,7-9,22 * * * ...
      */5 12-14 * * * ...
      @hourly ...
    CRONTAB

    parslet.parse(c)
  end
end
