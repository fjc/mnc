# frozen_string_literal: true

require 'test_helper'

class MNCRegexTest < Minitest::Test
  def test_it_does_something_useful
    load 'mnc/regex.rb'

    c = <<~CRONTAB
      00 * * * * ...
      30 6 * * * ...
      00,15,30,45 6,7,8,9,22 * * * ...
      42 12-14 * * * ...
    CRONTAB

    n = Time.parse('2021-06-20 06:25:00')
    assert_equal 300, MNC.new[now: n, crontab: c]

    n = Time.parse('2021-06-21 07:25:00')
    assert_equal 300, MNC.new[crontab: c, now: n]

    n = Time.parse('2021-06-22 07:35:00')
    assert_equal 600, MNC.new[crontab: c, now: n]

    n = Time.parse('2021-06-23 08:05:00')
    assert_equal 600, MNC.new[crontab: c, now: n]

    n = Time.parse('2021-06-24 23:05:00')
    assert_equal 3300, MNC.new[now: n, crontab: c]

    n = Time.parse('2021-06-25 11:41:00')
    assert_equal 1140, MNC.new[now: n, crontab: c]

    n = Time.parse('2021-06-26 12:41:00')
    assert_equal 60, MNC.new[now: n, crontab: c]

    n = Time.parse('2021-06-27 13:41:00')
    assert_equal 60, MNC.new[now: n, crontab: c]

    n = Time.parse('2021-06-28 14:41:00')
    assert_equal 60, MNC.new[now: n, crontab: c]

    n = Time.parse('2021-06-29 15:41:00')
    assert_equal 1140, MNC.new[now: n, crontab: c]
  end
end
