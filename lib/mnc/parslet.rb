# frozen_string_literal: true

require 'time'
require 'parslet'

class MNC
  def initialize
    @parser = Parslet.new
    @former = Parslet::Transform.new
  end

  def [](now: Time.now, crontab: `crontab -l`)
    Integer(
      @former.apply(@parser.parse(crontab))
      .map { |cron|
        cron[:minute].map { |min|
          cron[:hour].map { |hor|
            cron[:dom].map { |dom|
              cron[:month].map { |mon|
                cron[:dow].map { |dow|
                  c = [min, hor, dom, mon, dow].zip(%w[%M %H %d %m %w]).select { |a| a[0] }
                  t = c.map { |h| h[0] }.join(' ')
                  f = c.map { |h| h[1] }.join(' ')
                  n = now
                  n += 3600 while (_ = (Time.strptime t, f, n)) < now
                  n -=  600 while (_ = (Time.strptime t, f, n)) > now
                  n +=   60 while (r = (Time.strptime t, f, n)) < now
                  r
                }
              }
            }
          }
        }
      }.flatten.min - now
    )
  end

  class Parslet < Parslet::Parser
    rule(:crontab)     { lines.as(:crontab) >> eof }
    rule(:lines)       { (line >> eol).repeat }
    rule(:line)        { cron | env | comment | blank }

    rule(:space)       { match[" \t"].repeat(1) }
    rule(:eol)         { match["\r\n"].repeat(1) }
    rule(:eof)         { any.absent? }

    rule(:blank)       { space.repeat }

    rule(:comment)     { space >> hash >> match["^\n"].repeat }
    rule(:hash)        { str('#') }

    rule(:env)         { variable >> space >> equal >> space >> value }
    rule(:variable)    { match('\w').repeat(1) }
    rule(:equal)       { str('=') }
    rule(:value)       { match["^\n"].repeat(1) }

    rule(:cron)        { (nickname | traditional).as(:cron) >> space >> command }
    rule(:nickname)    { (str('@') >> match['a-z'].repeat(1)).as(:nickname) }
    rule(:traditional) { minute >> space >> hour >> space >> dom >> space >> month >> space >> dow }

    rule(:minute)      { entry.as(:minute) }
    rule(:hour)        { entry.as(:hour) }
    rule(:dom)         { entry.as(:dom) }
    rule(:month)       { entry.as(:month) }
    rule(:dow)         { entry.as(:dow) }

    rule(:entry)       { step | wildcard | list }
    rule(:step)        { str('*/') >> integer }
    rule(:wildcard)    { str('*') }
    rule(:list)        { (range | integer) >> (str(',') >> (range | integer)).repeat }
    rule(:range)       { integer >> str('-') >> integer }
    rule(:integer)     { match['0-9'].repeat(1) }
    rule(:command)     { match["^\n"].repeat(1) }

    root(:crontab)

    class Transform < ::Parslet::Transform
      ::Parslet::Context.class_eval do
        def _mnc__nickname_to_traditional(nn)
          minute, hour, dom, month, dow = %w[* * * * *]

          case nn
          when '@hourly'
            minute = 0
          when '@daily'
            minute, hour = 0, 0
          when '@weekly'
            minute, hour, dow = 0, 0, 0
          when '@monthly'
            minute, hour, dom = 0, 0, 1
          when '@yearly', '@annually'
            minute, hour, dom, month = 0, 0, 1, 1
          else
            return nil
          end

          {
            minute: minute, hour: hour, dom: dom, month: month, dow: dow
          }
        end

        def _mnc__convert_cron_ranges(cf)
          cf.to_s.split(',')
            .map { |v|
              v1, v2 = v.split('-').map(&:to_i)
              v2 ? v1..v2 : v1
            }
        end
      end

      rule(nickname: simple(:nn)) { _mnc__nickname_to_traditional(nn) }

      rule(crontab: subtree(:ct)) {
        ct.map { |c| c[:cron] }.compact.map { |c|
          {}.tap { |h|
            %i[minute hour dom month dow].each { |f|
              h[f] = c[f] == '*' ? [nil] : _mnc__convert_cron_ranges(c[f])
            }
          }.transform_values { |w|
            w.map { |v| [(Range === v ? v.to_a : v)] }.flatten.sort.uniq
          }
        }
      }
    end
  end
end
