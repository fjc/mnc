# frozen_string_literal: true

require 'time'

class MNC
  def initialize; end

  def [](now: Time.now, crontab: `crontab -l`)
    Integer(
      crontab
      .lines
      .select { |l| l =~ /^(?:[-0-9,*]+\s+){5}\S+/ }
      .map { |l| l.split.zip %w[%M %H %d %m %w] }
      .map { |a| a[0..4].reject { |b| b[0] == '*' } }
      .map { |a|
        a.map { |b|
          b[0].split(',').map { |c| [c, b[1]] }
        }.map { |b|
          b.map { |c|
            d = c[0].split('-')
            (d[0]..(d[1] || d[0])).map { |e| [e, c[1]] }
          }.flatten(1)
        }
      }
      .map { |a|
        (a[0] || [nil]).map { |b|
          (a[1] || [nil]).map { |c|
            (a[2] || [nil]).map { |d|
              (a[3] || [nil]).map { |e|
                (a[4] || [nil]).map { |f|
                  g = [b, c, d, e, f].compact
                  t = g.map { |h| h[0] }.join(' ')
                  f = g.map { |h| h[1] }.join(' ')
                  n = now
                  n += 3600 while (_ = (Time.strptime t, f, n)) < now
                  n -=  600 while (_ = (Time.strptime t, f, n)) > now
                  n +=   60 while (r = (Time.strptime t, f, n)) < now
                  r
                }
              }
            }
          }
        }
      }.flatten.min - now
    )
  end
end
